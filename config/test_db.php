<?php
$db = require __DIR__ . '/db.php';

// test database! Important not to run tests on production or development databases

$DB_NAME = 'todo_test';

$DB_HOST = '127.0.0.1';
// Gitlab-CI uses mysql for hostname
if ( getenv('GITLAB_CI') == 'true' ){
  $DB_HOST = 'mysql';
}

// if ( getenv("CI") == 'true' ){ // not tested

  // PostgreSQL (not tested)
  // $db['class'] = 'yii\db\Connection';
  // $db['dsn'] = 'pgsql:host=localhost:5432;dbname='.$DB_NAME;
  // $db['username'] = 'postgres';
  // $db['password'] = '';
  // $db['charset'] = 'utf8';

// } else {
  // MySQL
  $db['dsn'] = "mysql:host=$DB_HOST:3306;dbname=$DB_NAME";
  $db['charset'] = 'utf8';

// }

return $db;
